/*
 * File: app/view/WritingSerchPanel.js
 *
 * This file was generated by Sencha Architect version 3.1.0.
 * http://www.sencha.com/products/architect/
 *
 * This file requires use of the Sencha Touch 2.3.x library, under independent license.
 * License of Sencha Architect does not include license for Sencha Touch 2.3.x. For more
 * details see http://www.sencha.com/license or contact license@sencha.com.
 *
 * This file will be auto-generated each and everytime you save your project.
 *
 * Do NOT hand edit this file.
 */

Ext.define('ChinesEcho.view.WritingSerchPanel', {
    extend: 'Ext.Panel',

    requires: [
        'Ext.TitleBar',
        'Ext.Panel',
        'Ext.dataview.DataView',
        'Ext.XTemplate',
        'Ext.Button'
    ],

    config: {
        centered: true,
        height: '80%',
        html: '',
        itemId: 'WritingPanel',
        hideOnMaskTap: true,
        layout: 'vbox',
        modal: true,
        items: [
            {
                xtype: 'titlebar',
                docked: 'top',
                hideOnMaskTap: false,
                title: '필기인식'
            },
            {
                xtype: 'panel',
                html: ' <center><canvas id="webcanvas" width=300  height=300>test</canvas></center>'
            },
            {
                xtype: 'dataview',
                flex: 1,
                itemId: 'checkWordView',
                inline: true,
                itemTpl: [
                    '<center>',
                    '    <div class="writingSerchResult">',
                    '        {chinese}<br>',
                    '        <font size=2>{pronunciation}</font>',
                    '    </div>',
                    '</center>'
                ]
            },
            {
                xtype: 'panel',
                docked: 'bottom',
                layout: 'hbox',
                items: [
                    {
                        xtype: 'button',
                        flex: 1,
                        itemId: 'mybutton29',
                        text: '검색'
                    },
                    {
                        xtype: 'button',
                        flex: 1,
                        itemId: 'mybutton17',
                        text: '지우기'
                    }
                ]
            }
        ],
        listeners: [
            {
                fn: 'onWritingSerchPanelPainted1',
                event: 'painted'
            },
            {
                fn: 'onWritingPanelHide',
                event: 'hide'
            }
        ]
    },

    onWritingSerchPanelPainted1: function(element, eOpts) {
        if(!webcanvas){
            var canvas = document.getElementById("webcanvas");
            if (canvas.getContext) {
                webcanvas = new WebCanvas(canvas);
                console.log(webcanvas);
                webcanvas.draw();

            }
        }
    },

    onWritingPanelHide: function(component, eOpts) {
        component.destroy();
        webcanvas = null;
    }

});
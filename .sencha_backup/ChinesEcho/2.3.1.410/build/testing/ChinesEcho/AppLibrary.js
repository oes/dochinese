function _546647b6dd084aeed311da314514e8f4ee8e97bd(){};function checkEmail(email){
  var regExp = /[0-9a-zA-Z][_0-9a-zA-Z-]*@[_0-9a-zA-Z-]+(\.[_0-9a-zA-Z-]+){1,2}$/;
  return regExp.test(email);
}

function checkPassword(password){
    if(password.length < 8) return false;
    return true;
}

var webcanvas = null;
function writingToSexp(writing) {
                var cchar = new Character();
                cchar.setUTF8("?");
                cchar.setWriting(writing);
                return cchar.toSexp();
            }       
            function canvasLoad() {
                var canvas = document.getElementById("webcanvas");
    
                if (canvas.getContext) {
                    webcanvas = new WebCanvas(canvas);
                    webcanvas.draw();
                }
            }

            function canvasToSexp() {
                if (webcanvas) {
                    var writing = webcanvas.getWriting();
                    writingToSexp(writing);
var url = "http://172.16.100.155:5000/api/v1.0/handwritingcheck?sexpData="+output_textarea.value;
                }
            }
            
    
            function canvasClear() {
                if (webcanvas) {
                    webcanvas.clear();
                }
            }
            
            function canvasRevertStroke() {
                if (webcanvas) {
                    webcanvas.revertStroke();
                }
            }            
            
            function canvasReplay() {
                if (webcanvas) {
                    webcanvas.replay();
                }
            }      

            function canvasSmooth() {
                if (webcanvas) {
                    webcanvas.smooth();
                }
            }

/**character.js***/
/*
* Copyright (C) 2008 The Tegaki project contributors
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License along
* with this program; if not, write to the Free Software Foundation, Inc.,
* 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

/* 
* Contributors to this file:
*  - Mathieu Blondel
*/

DEFAULT_WIDTH = 1000;
DEFAULT_HEIGHT = 1000;

/* Point */

var Point = function(x, y, pressure, xtilt, ytilt, timestamp) {
    this.x = x;
    this.y = y;
    this.pressure = pressure || null;
    this.xtilt = xtilt || null;
    this.ytilt = ytilt || null;
    this.timestamp = timestamp || null;
}

Point.prototype.copy_from = function(point) {
    var keys = ["x", "y", "pressure", "xtilt", "ytilt", "timestamp"];

    for (var i = 0; i < keys.length; i++) {
        var key = keys[i];

        if (point[key] != null)
            this[key] = point[key];
    }
}

Point.prototype.copy = function() {
    var c = new Point();
    c.copy_from(this);
    return c;
}

Point.prototype.toXML = function() {
    var values = [];
    var keys = ["x", "y", "pressure", "xtilt", "ytilt", "timestamp"];

    for (var i = 0; i < keys.length; i++) {
        var key = keys[i];
        if (this[key] != null)
            values.push(key + "=\"" + this[key] + "\"");
    }

    return "<point " + values.join(" ") + " />";
}

Point.prototype.toSexp = function() {
    return "(" + this["x"] + " "+ this["y"] + ")";
}

/* Stroke */

var Stroke = function() {
    this.points = [];
    this.is_smoothed = false;
}

Stroke.prototype.copy_from = function(stroke) {
    for(var i = 0; i < stroke.points.length; i++) {
        var point = new Point();
        point.copy_from(stroke.points[i]);
        this.points[i] = point;
    }
    this.points.length = stroke.points.length;
}

Stroke.prototype.copy = function() {
    var c = new Stroke();
    c.copy_from(this);
    return c;
}

Stroke.prototype.getPoints = function() {
    return this.points;
}

Stroke.prototype.getNPoints = function() {
    return this.points.length;
}

Stroke.prototype.getDuration = function() {
    if (this.points.length > 0) {
        last = this.points.length - 1;

        if (this.points[last].timestamp != null && this.points[0].timestamp !=
null)
            return (this.points[last].timestamp - this.points[0].timestamp);
    }
    return null;
};

Stroke.prototype.appendPoint = function(point) {
    this.points.push(point);
};

Stroke.prototype.toXML = function() {
    var s = "<stroke>\n";

    for (var i=0; i < this.points.length; i++)
        s += "  " + this.points[i].toXML() + "\n";

    s += "</stroke>";

    return s;
}

Stroke.prototype.toSexp = function() {
    var s = "(";

    for (var i=0; i < this.points.length; i++)
        s += "  " + this.points[i].toSexp() + "\n";

    s += ")";

    return s;
}

Stroke.prototype.smooth = function() {
    /* Smoothing method based on a (simple) moving average algorithm. 
     *
     * Let p = p(0), ..., p(N) be the set points of this stroke, 
     *     w = w(-M), ..., w(0), ..., w(M) be a set of weights.
     *
     * This algorithm aims at replacing p with a set p' such as
     *
     *    p'(i) = (w(-M)*p(i-M) + ... + w(0)*p(i) + ... + w(M)*p(i+M)) / S
     *
     * and where S = w(-M) + ... + w(0) + ... w(M). End points are not
     * affected.
     */

    if (this.is_smoothed)
        return;

    var weights = [1, 1, 2, 1, 1]; // Weights to be used
    var times = 3;                 // Number of times to apply the algorithm

    if (this.points.length >= weights.length) {
        var offset = Math.floor(weights.length / 2);
        var sum = 0;

        for (var j = 0; j < weights.length; j++) {
            sum += weights[j];
        }

        for (var n = 1; n <= times; n++) {
            var s = this.copy();

            for (var i = offset; i < this.points.length - offset; i++) {
                this.points[i].x = 0;
                this.points[i].y = 0;
                
                for (var j = 0; j < weights.length; j++) {
                    this.points[i].x += weights[j] * s.points[i + j - offset].x;
                    this.points[i].y += weights[j] * s.points[i + j - offset].y;
                }

                this.points[i].x = Math.round(this.points[i].x / sum);
                this.points[i].y = Math.round(this.points[i].y / sum);
            }
        }
    }
    this.is_smoothed = true;
}

/* Writing */

var Writing = function() {
    this.strokes = [];
    this.width = DEFAULT_WIDTH;
    this.height = DEFAULT_HEIGHT;
}

Writing.prototype.copy_from = function(writing) {
    for(var i = 0; i < writing.strokes.length; i++) {
        var stroke = new Stroke();
        stroke.copy_from(writing.strokes[i]);
        this.strokes[i] = stroke;
    }
    this.strokes.length = writing.strokes.length;
}

Writing.prototype.copy = function() {
    var c = new Writing();
    c.copy_from(this);
    return c;
}

Writing.prototype.getDuration = function() {
    var last = this.strokes.length - 1;
    var lastp = this.strokes[last].getPoints().length - 1;
    if (this.strokes.length > 0)
        if (this.strokes[0].getPoints()[0].timestamp != null &&
            this.strokes[last].getPoints()[lastp].timestamp != null)
            return (this.strokes[last].getPoints()[lastp].timestamp -
                    this.strokes[0].getPoints()[0].timestamp);
    return null;
}

Writing.prototype.getNStrokes = function() {
    return this.strokes.length;
}

Writing.prototype.getStrokes = function() {
    return this.strokes;
}

Writing.prototype.moveToPoint = function(point) {
    var stroke = new Stroke();     
    stroke.appendPoint(point);
    this.appendStroke(stroke);
}

Writing.prototype.lineToPoint = function(point) {
    this.strokes[this.strokes.length - 1].appendPoint(point);
}
        
Writing.prototype.appendStroke = function(stroke) {
    this.strokes.push(stroke);
}

Writing.prototype.removeLastStroke = function() {
    if (this.strokes.length > 0)
        this.strokes.pop();
}

Writing.prototype.clear = function() {
    this.strokes = [];
}

Writing.prototype.toXML = function() {
    var s = "<width>" + this.width + "</width>\n"
    s += "<height>" + this.height + "</height>\n"

    s += "<strokes>\n";

    for (var i = 0; i < this.strokes.length; i++) {
        var lines = this.strokes[i].toXML().split("\n");
        
        for (var j = 0; j < lines.length; j++)
            s += "  " + lines[j] + "\n";
    }

    s += "</strokes>";

    return s;
}

Writing.prototype.toSexp = function() {
    var s = "(width " + this.width + ") "
    s += "(height " + this.height + ")\n"

    s += "(strokes ";

    for (var i = 0; i < this.strokes.length; i++) {
        var lines = this.strokes[i].toSexp().split("\n");
        
        for (var j = 0; j < lines.length; j++)
            s += " " + lines[j] + "";
    }

    s += ")";

    return s;
}

Writing.prototype.smooth = function() {
    for (var i = 0; i < this.strokes.length; i++) {
        this.strokes[i].smooth();
    }
}

/* Character */

var Character = function() {
    this.writing = new Writing();
    this.utf8 = null;
}

Character.prototype.copy_from = function(character) {
    this.setUTF8(character.utf8);

    var writing = new Writing();
    writing.copy_from(character.writing);

    this.setWriting(writing);
}

Character.prototype.copy = function() {
    var c = new Character();
    c.copy_from(this);
    return c;
}

Character.prototype.getUTF8 = function() {
    return this.utf8;
}

Character.prototype.setUTF8 = function(utf8) {
    this.utf8 = utf8;
}

Character.prototype.getWriting = function() {
    return this.writing;
}

Character.prototype.setWriting = function(writing) {
    this.writing = writing;
}

Character.prototype.toSexp = function() {
    var s = "(character";

    var lines = this.writing.toSexp().split("\n");

    for (var i = 0; i < lines.length; i++)
        s += " " + lines[i] + " ";

    s += ")";

    return s;
}

Character.prototype.toXML = function() {
    var s = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";

    s += "<character>\n";
    s += "  <utf8>" + this.utf8 + "</utf8>\n";

    var lines = this.writing.toXML().split("\n");

    for (var i = 0; i < lines.length; i++)
        s += "  " + lines[i] + "\n";

    s += "</character>";

    return s;
}


/**excanvas.js***/
// Copyright 2006 Google Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


// Known Issues:
//
// * Patterns are not implemented.
// * Radial gradient are not implemented. The VML version of these look very
//   different from the canvas one.
// * Clipping paths are not implemented.
// * Coordsize. The width and height attribute have higher priority than the
//   width and height style values which isn't correct.
// * Painting mode isn't implemented.
// * Canvas width/height should is using content-box by default. IE in
//   Quirks mode will draw the canvas using border-box. Either change your
//   doctype to HTML5
//   (http://www.whatwg.org/specs/web-apps/current-work/#the-doctype)
//   or use Box Sizing Behavior from WebFX
//   (http://webfx.eae.net/dhtml/boxsizing/boxsizing.html)
// * Optimize. There is always room for speed improvements.

// only add this code if we do not already have a canvas implementation
if (!window.CanvasRenderingContext2D) {

(function () {

  // alias some functions to make (compiled) code shorter
  var m = Math;
  var mr = m.round;
  var ms = m.sin;
  var mc = m.cos;

  // this is used for sub pixel precision
  var Z = 10;
  var Z2 = Z / 2;

  var G_vmlCanvasManager_ = {
    init: function (opt_doc) {
      var doc = opt_doc || document;
      if (/MSIE/.test(navigator.userAgent) && !window.opera) {
        var self = this;
        doc.attachEvent("onreadystatechange", function () {
          self.init_(doc);
        });
      }
    },

    init_: function (doc) {
      if (doc.readyState == "complete") {
        // create xmlns
        if (!doc.namespaces["g_vml_"]) {
          doc.namespaces.add("g_vml_", "urn:schemas-microsoft-com:vml");
        }

        // setup default css
        var ss = doc.createStyleSheet();
        ss.cssText = "canvas{display:inline-block;overflow:hidden;" +
            // default size is 300x150 in Gecko and Opera
            "text-align:left;width:300px;height:150px}" +
            "g_vml_\\:*{behavior:url(#default#VML)}";

        // find all canvas elements
        var els = doc.getElementsByTagName("canvas");
        for (var i = 0; i < els.length; i++) {
          if (!els[i].getContext) {
            this.initElement(els[i]);
          }
        }
      }
    },

    fixElement_: function (el) {
      // in IE before version 5.5 we would need to add HTML: to the tag name
      // but we do not care about IE before version 6
      var outerHTML = el.outerHTML;

      var newEl = el.ownerDocument.createElement(outerHTML);
      // if the tag is still open IE has created the children as siblings and
      // it has also created a tag with the name "/FOO"
      if (outerHTML.slice(-2) != "/>") {
        var tagName = "/" + el.tagName;
        var ns;
        // remove content
        while ((ns = el.nextSibling) && ns.tagName != tagName) {
          ns.removeNode();
        }
        // remove the incorrect closing tag
        if (ns) {
          ns.removeNode();
        }
      }
      el.parentNode.replaceChild(newEl, el);
      return newEl;
    },

    /**
     * Public initializes a canvas element so that it can be used as canvas
     * element from now on. This is called automatically before the page is
     * loaded but if you are creating elements using createElement you need to
     * make sure this is called on the element.
     * @param {HTMLElement} el The canvas element to initialize.
     * @return {HTMLElement} the element that was created.
     */
    initElement: function (el) {
      el = this.fixElement_(el);
      el.getContext = function () {
        if (this.context_) {
          return this.context_;
        }
        return this.context_ = new CanvasRenderingContext2D_(this);
      };

      // do not use inline function because that will leak memory
      el.attachEvent('onpropertychange', onPropertyChange);
      el.attachEvent('onresize', onResize);

      var attrs = el.attributes;
      if (attrs.width && attrs.width.specified) {
        // TODO: use runtimeStyle and coordsize
        // el.getContext().setWidth_(attrs.width.nodeValue);
        el.style.width = attrs.width.nodeValue + "px";
      } else {
        el.width = el.clientWidth;
      }
      if (attrs.height && attrs.height.specified) {
        // TODO: use runtimeStyle and coordsize
        // el.getContext().setHeight_(attrs.height.nodeValue);
        el.style.height = attrs.height.nodeValue + "px";
      } else {
        el.height = el.clientHeight;
      }
      //el.getContext().setCoordsize_()
      return el;
    }
  };

  function onPropertyChange(e) {
    var el = e.srcElement;

    switch (e.propertyName) {
      case 'width':
        el.style.width = el.attributes.width.nodeValue + "px";
        el.getContext().clearRect();
        break;
      case 'height':
        el.style.height = el.attributes.height.nodeValue + "px";
        el.getContext().clearRect();
        break;
    }
  }

  function onResize(e) {
    var el = e.srcElement;
    if (el.firstChild) {
      el.firstChild.style.width =  el.clientWidth + 'px';
      el.firstChild.style.height = el.clientHeight + 'px';
    }
  }

  G_vmlCanvasManager_.init();

  // precompute "00" to "FF"
  var dec2hex = [];
  for (var i = 0; i < 16; i++) {
    for (var j = 0; j < 16; j++) {
      dec2hex[i * 16 + j] = i.toString(16) + j.toString(16);
    }
  }

  function createMatrixIdentity() {
    return [
      [1, 0, 0],
      [0, 1, 0],
      [0, 0, 1]
    ];
  }

  function matrixMultiply(m1, m2) {
    var result = createMatrixIdentity();

    for (var x = 0; x < 3; x++) {
      for (var y = 0; y < 3; y++) {
        var sum = 0;

        for (var z = 0; z < 3; z++) {
          sum += m1[x][z] * m2[z][y];
        }

        result[x][y] = sum;
      }
    }
    return result;
  }

  function copyState(o1, o2) {
    o2.fillStyle     = o1.fillStyle;
    o2.lineCap       = o1.lineCap;
    o2.lineJoin      = o1.lineJoin;
    o2.lineWidth     = o1.lineWidth;
    o2.miterLimit    = o1.miterLimit;
    o2.shadowBlur    = o1.shadowBlur;
    o2.shadowColor   = o1.shadowColor;
    o2.shadowOffsetX = o1.shadowOffsetX;
    o2.shadowOffsetY = o1.shadowOffsetY;
    o2.strokeStyle   = o1.strokeStyle;
    o2.arcScaleX_    = o1.arcScaleX_;
    o2.arcScaleY_    = o1.arcScaleY_;
  }

  function processStyle(styleString) {
    var str, alpha = 1;

    styleString = String(styleString);
    if (styleString.substring(0, 3) == "rgb") {
      var start = styleString.indexOf("(", 3);
      var end = styleString.indexOf(")", start + 1);
      var guts = styleString.substring(start + 1, end).split(",");

      str = "#";
      for (var i = 0; i < 3; i++) {
        str += dec2hex[Number(guts[i])];
      }

      if ((guts.length == 4) && (styleString.substr(3, 1) == "a")) {
        alpha = guts[3];
      }
    } else {
      str = styleString;
    }

    return [str, alpha];
  }

  function processLineCap(lineCap) {
    switch (lineCap) {
      case "butt":
        return "flat";
      case "round":
        return "round";
      case "square":
      default:
        return "square";
    }
  }

  /**
   * This class implements CanvasRenderingContext2D interface as described by
   * the WHATWG.
   * @param {HTMLElement} surfaceElement The element that the 2D context should
   * be associated with
   */
   function CanvasRenderingContext2D_(surfaceElement) {
    this.m_ = createMatrixIdentity();

    this.mStack_ = [];
    this.aStack_ = [];
    this.currentPath_ = [];

    // Canvas context properties
    this.strokeStyle = "#000";
    this.fillStyle = "#000";

    this.lineWidth = 1;
    this.lineJoin = "miter";
    this.lineCap = "butt";
    this.miterLimit = Z * 1;
    this.globalAlpha = 1;
    this.canvas = surfaceElement;

    var el = surfaceElement.ownerDocument.createElement('div');
    el.style.width =  surfaceElement.clientWidth + 'px';
    el.style.height = surfaceElement.clientHeight + 'px';
    el.style.overflow = 'hidden';
    el.style.position = 'absolute';
    surfaceElement.appendChild(el);

    this.element_ = el;
    this.arcScaleX_ = 1;
    this.arcScaleY_ = 1;
  };

  var contextPrototype = CanvasRenderingContext2D_.prototype;
  contextPrototype.clearRect = function() {
    this.element_.innerHTML = "";
    this.currentPath_ = [];
  };

  contextPrototype.beginPath = function() {
    // TODO: Branch current matrix so that save/restore has no effect
    //       as per safari docs.

    this.currentPath_ = [];
  };

  contextPrototype.moveTo = function(aX, aY) {
    this.currentPath_.push({type: "moveTo", x: aX, y: aY});
    this.currentX_ = aX;
    this.currentY_ = aY;
  };

  contextPrototype.lineTo = function(aX, aY) {
    this.currentPath_.push({type: "lineTo", x: aX, y: aY});
    this.currentX_ = aX;
    this.currentY_ = aY;
  };

  contextPrototype.bezierCurveTo = function(aCP1x, aCP1y,
                                            aCP2x, aCP2y,
                                            aX, aY) {
    this.currentPath_.push({type: "bezierCurveTo",
                           cp1x: aCP1x,
                           cp1y: aCP1y,
                           cp2x: aCP2x,
                           cp2y: aCP2y,
                           x: aX,
                           y: aY});
    this.currentX_ = aX;
    this.currentY_ = aY;
  };

  contextPrototype.quadraticCurveTo = function(aCPx, aCPy, aX, aY) {
    // the following is lifted almost directly from
    // http://developer.mozilla.org/en/docs/Canvas_tutorial:Drawing_shapes
    var cp1x = this.currentX_ + 2.0 / 3.0 * (aCPx - this.currentX_);
    var cp1y = this.currentY_ + 2.0 / 3.0 * (aCPy - this.currentY_);
    var cp2x = cp1x + (aX - this.currentX_) / 3.0;
    var cp2y = cp1y + (aY - this.currentY_) / 3.0;
    this.bezierCurveTo(cp1x, cp1y, cp2x, cp2y, aX, aY);
  };

  contextPrototype.arc = function(aX, aY, aRadius,
                                  aStartAngle, aEndAngle, aClockwise) {
    aRadius *= Z;
    var arcType = aClockwise ? "at" : "wa";

    var xStart = aX + (mc(aStartAngle) * aRadius) - Z2;
    var yStart = aY + (ms(aStartAngle) * aRadius) - Z2;

    var xEnd = aX + (mc(aEndAngle) * aRadius) - Z2;
    var yEnd = aY + (ms(aEndAngle) * aRadius) - Z2;

    // IE won't render arches drawn counter clockwise if xStart == xEnd.
    if (xStart == xEnd && !aClockwise) {
      xStart += 0.125; // Offset xStart by 1/80 of a pixel. Use something
                       // that can be represented in binary
    }

    this.currentPath_.push({type: arcType,
                           x: aX,
                           y: aY,
                           radius: aRadius,
                           xStart: xStart,
                           yStart: yStart,
                           xEnd: xEnd,
                           yEnd: yEnd});

  };

  contextPrototype.rect = function(aX, aY, aWidth, aHeight) {
    this.moveTo(aX, aY);
    this.lineTo(aX + aWidth, aY);
    this.lineTo(aX + aWidth, aY + aHeight);
    this.lineTo(aX, aY + aHeight);
    this.closePath();
  };

  contextPrototype.strokeRect = function(aX, aY, aWidth, aHeight) {
    // Will destroy any existing path (same as FF behaviour)
    this.beginPath();
    this.moveTo(aX, aY);
    this.lineTo(aX + aWidth, aY);
    this.lineTo(aX + aWidth, aY + aHeight);
    this.lineTo(aX, aY + aHeight);
    this.closePath();
    this.stroke();
  };

  contextPrototype.fillRect = function(aX, aY, aWidth, aHeight) {
    // Will destroy any existing path (same as FF behaviour)
    this.beginPath();
    this.moveTo(aX, aY);
    this.lineTo(aX + aWidth, aY);
    this.lineTo(aX + aWidth, aY + aHeight);
    this.lineTo(aX, aY + aHeight);
    this.closePath();
    this.fill();
  };

  contextPrototype.createLinearGradient = function(aX0, aY0, aX1, aY1) {
    var gradient = new CanvasGradient_("gradient");
    return gradient;
  };

  contextPrototype.createRadialGradient = function(aX0, aY0,
                                                   aR0, aX1,
                                                   aY1, aR1) {
    var gradient = new CanvasGradient_("gradientradial");
    gradient.radius1_ = aR0;
    gradient.radius2_ = aR1;
    gradient.focus_.x = aX0;
    gradient.focus_.y = aY0;
    return gradient;
  };

  contextPrototype.drawImage = function (image, var_args) {
    var dx, dy, dw, dh, sx, sy, sw, sh;

    // to find the original width we overide the width and height
    var oldRuntimeWidth = image.runtimeStyle.width;
    var oldRuntimeHeight = image.runtimeStyle.height;
    image.runtimeStyle.width = 'auto';
    image.runtimeStyle.height = 'auto';

    // get the original size
    var w = image.width;
    var h = image.height;

    // and remove overides
    image.runtimeStyle.width = oldRuntimeWidth;
    image.runtimeStyle.height = oldRuntimeHeight;

    if (arguments.length == 3) {
      dx = arguments[1];
      dy = arguments[2];
      sx = sy = 0;
      sw = dw = w;
      sh = dh = h;
    } else if (arguments.length == 5) {
      dx = arguments[1];
      dy = arguments[2];
      dw = arguments[3];
      dh = arguments[4];
      sx = sy = 0;
      sw = w;
      sh = h;
    } else if (arguments.length == 9) {
      sx = arguments[1];
      sy = arguments[2];
      sw = arguments[3];
      sh = arguments[4];
      dx = arguments[5];
      dy = arguments[6];
      dw = arguments[7];
      dh = arguments[8];
    } else {
      throw "Invalid number of arguments";
    }

    var d = this.getCoords_(dx, dy);

    var w2 = sw / 2;
    var h2 = sh / 2;

    var vmlStr = [];

    var W = 10;
    var H = 10;

    // For some reason that I've now forgotten, using divs didn't work
    vmlStr.push(' <g_vml_:group',
                ' coordsize="', Z * W, ',', Z * H, '"',
                ' coordorigin="0,0"' ,
                ' style="width:', W, ';height:', H, ';position:absolute;');

    // If filters are necessary (rotation exists), create them
    // filters are bog-slow, so only create them if abbsolutely necessary
    // The following check doesn't account for skews (which don't exist
    // in the canvas spec (yet) anyway.

    if (this.m_[0][0] != 1 || this.m_[0][1]) {
      var filter = [];

      // Note the 12/21 reversal
      filter.push("M11='", this.m_[0][0], "',",
                  "M12='", this.m_[1][0], "',",
                  "M21='", this.m_[0][1], "',",
                  "M22='", this.m_[1][1], "',",
                  "Dx='", mr(d.x / Z), "',",
                  "Dy='", mr(d.y / Z), "'");

      // Bounding box calculation (need to minimize displayed area so that
      // filters don't waste time on unused pixels.
      var max = d;
      var c2 = this.getCoords_(dx + dw, dy);
      var c3 = this.getCoords_(dx, dy + dh);
      var c4 = this.getCoords_(dx + dw, dy + dh);

      max.x = Math.max(max.x, c2.x, c3.x, c4.x);
      max.y = Math.max(max.y, c2.y, c3.y, c4.y);

      vmlStr.push("padding:0 ", mr(max.x / Z), "px ", mr(max.y / Z),
                  "px 0;filter:progid:DXImageTransform.Microsoft.Matrix(",
                  filter.join(""), ", sizingmethod='clip');")
    } else {
      vmlStr.push("top:", mr(d.y / Z), "px;left:", mr(d.x / Z), "px;")
    }

    vmlStr.push(' ">' ,
                '<g_vml_:image src="', image.src, '"',
                ' style="width:', Z * dw, ';',
                ' height:', Z * dh, ';"',
                ' cropleft="', sx / w, '"',
                ' croptop="', sy / h, '"',
                ' cropright="', (w - sx - sw) / w, '"',
                ' cropbottom="', (h - sy - sh) / h, '"',
                ' />',
                '</g_vml_:group>');

    this.element_.insertAdjacentHTML("BeforeEnd",
                                    vmlStr.join(""));
  };

  contextPrototype.stroke = function(aFill) {
    var lineStr = [];
    var lineOpen = false;
    var a = processStyle(aFill ? this.fillStyle : this.strokeStyle);
    var color = a[0];
    var opacity = a[1] * this.globalAlpha;

    var W = 10;
    var H = 10;

    lineStr.push('<g_vml_:shape',
                 ' fillcolor="', color, '"',
                 ' filled="', Boolean(aFill), '"',
                 ' style="position:absolute;width:', W, ';height:', H, ';"',
                 ' coordorigin="0 0" coordsize="', Z * W, ' ', Z * H, '"',
                 ' stroked="', !aFill, '"',
                 ' strokeweight="', this.lineWidth, '"',
                 ' strokecolor="', color, '"',
                 ' path="');

    var newSeq = false;
    var min = {x: null, y: null};
    var max = {x: null, y: null};

    for (var i = 0; i < this.currentPath_.length; i++) {
      var p = this.currentPath_[i];

      if (p.type == "moveTo") {
        lineStr.push(" m ");
        var c = this.getCoords_(p.x, p.y);
        lineStr.push(mr(c.x), ",", mr(c.y));
      } else if (p.type == "lineTo") {
        lineStr.push(" l ");
        var c = this.getCoords_(p.x, p.y);
        lineStr.push(mr(c.x), ",", mr(c.y));
      } else if (p.type == "close") {
        lineStr.push(" x ");
      } else if (p.type == "bezierCurveTo") {
        lineStr.push(" c ");
        var c = this.getCoords_(p.x, p.y);
        var c1 = this.getCoords_(p.cp1x, p.cp1y);
        var c2 = this.getCoords_(p.cp2x, p.cp2y);
        lineStr.push(mr(c1.x), ",", mr(c1.y), ",",
                     mr(c2.x), ",", mr(c2.y), ",",
                     mr(c.x), ",", mr(c.y));
      } else if (p.type == "at" || p.type == "wa") {
        lineStr.push(" ", p.type, " ");
        var c  = this.getCoords_(p.x, p.y);
        var cStart = this.getCoords_(p.xStart, p.yStart);
        var cEnd = this.getCoords_(p.xEnd, p.yEnd);

        lineStr.push(mr(c.x - this.arcScaleX_ * p.radius), ",",
                     mr(c.y - this.arcScaleY_ * p.radius), " ",
                     mr(c.x + this.arcScaleX_ * p.radius), ",",
                     mr(c.y + this.arcScaleY_ * p.radius), " ",
                     mr(cStart.x), ",", mr(cStart.y), " ",
                     mr(cEnd.x), ",", mr(cEnd.y));
      }


      // TODO: Following is broken for curves due to
      //       move to proper paths.

      // Figure out dimensions so we can do gradient fills
      // properly
      if(c) {
        if (min.x == null || c.x < min.x) {
          min.x = c.x;
        }
        if (max.x == null || c.x > max.x) {
          max.x = c.x;
        }
        if (min.y == null || c.y < min.y) {
          min.y = c.y;
        }
        if (max.y == null || c.y > max.y) {
          max.y = c.y;
        }
      }
    }
    lineStr.push(' ">');

    if (typeof this.fillStyle == "object") {
      var focus = {x: "50%", y: "50%"};
      var width = (max.x - min.x);
      var height = (max.y - min.y);
      var dimension = (width > height) ? width : height;

      focus.x = mr((this.fillStyle.focus_.x / width) * 100 + 50) + "%";
      focus.y = mr((this.fillStyle.focus_.y / height) * 100 + 50) + "%";

      var colors = [];

      // inside radius (%)
      if (this.fillStyle.type_ == "gradientradial") {
        var inside = (this.fillStyle.radius1_ / dimension * 100);

        // percentage that outside radius exceeds inside radius
        var expansion = (this.fillStyle.radius2_ / dimension * 100) - inside;
      } else {
        var inside = 0;
        var expansion = 100;
      }

      var insidecolor = {offset: null, color: null};
      var outsidecolor = {offset: null, color: null};

      // We need to sort 'colors' by percentage, from 0 > 100 otherwise ie
      // won't interpret it correctly
      this.fillStyle.colors_.sort(function (cs1, cs2) {
        return cs1.offset - cs2.offset;
      });

      for (var i = 0; i < this.fillStyle.colors_.length; i++) {
        var fs = this.fillStyle.colors_[i];

        colors.push( (fs.offset * expansion) + inside, "% ", fs.color, ",");

        if (fs.offset > insidecolor.offset || insidecolor.offset == null) {
          insidecolor.offset = fs.offset;
          insidecolor.color = fs.color;
        }

        if (fs.offset < outsidecolor.offset || outsidecolor.offset == null) {
          outsidecolor.offset = fs.offset;
          outsidecolor.color = fs.color;
        }
      }
      colors.pop();

      lineStr.push('<g_vml_:fill',
                   ' color="', outsidecolor.color, '"',
                   ' color2="', insidecolor.color, '"',
                   ' type="', this.fillStyle.type_, '"',
                   ' focusposition="', focus.x, ', ', focus.y, '"',
                   ' colors="', colors.join(""), '"',
                   ' opacity="', opacity, '" />');
    } else if (aFill) {
      lineStr.push('<g_vml_:fill color="', color, '" opacity="', opacity, '" />');
    } else {
      lineStr.push(
        '<g_vml_:stroke',
        ' opacity="', opacity,'"',
        ' joinstyle="', this.lineJoin, '"',
        ' miterlimit="', this.miterLimit, '"',
        ' endcap="', processLineCap(this.lineCap) ,'"',
        ' weight="', this.lineWidth, 'px"',
        ' color="', color,'" />'
      );
    }

    lineStr.push("</g_vml_:shape>");

    this.element_.insertAdjacentHTML("beforeEnd", lineStr.join(""));

    this.currentPath_ = [];
  };

  contextPrototype.fill = function() {
    this.stroke(true);
  }

  contextPrototype.closePath = function() {
    this.currentPath_.push({type: "close"});
  };

  /**
   * @private
   */
  contextPrototype.getCoords_ = function(aX, aY) {
    return {
      x: Z * (aX * this.m_[0][0] + aY * this.m_[1][0] + this.m_[2][0]) - Z2,
      y: Z * (aX * this.m_[0][1] + aY * this.m_[1][1] + this.m_[2][1]) - Z2
    }
  };

  contextPrototype.save = function() {
    var o = {};
    copyState(this, o);
    this.aStack_.push(o);
    this.mStack_.push(this.m_);
    this.m_ = matrixMultiply(createMatrixIdentity(), this.m_);
  };

  contextPrototype.restore = function() {
    copyState(this.aStack_.pop(), this);
    this.m_ = this.mStack_.pop();
  };

  contextPrototype.translate = function(aX, aY) {
    var m1 = [
      [1,  0,  0],
      [0,  1,  0],
      [aX, aY, 1]
    ];

    this.m_ = matrixMultiply(m1, this.m_);
  };

  contextPrototype.rotate = function(aRot) {
    var c = mc(aRot);
    var s = ms(aRot);

    var m1 = [
      [c,  s, 0],
      [-s, c, 0],
      [0,  0, 1]
    ];

    this.m_ = matrixMultiply(m1, this.m_);
  };

  contextPrototype.scale = function(aX, aY) {
    this.arcScaleX_ *= aX;
    this.arcScaleY_ *= aY;
    var m1 = [
      [aX, 0,  0],
      [0,  aY, 0],
      [0,  0,  1]
    ];

    this.m_ = matrixMultiply(m1, this.m_);
  };

  /******** STUBS ********/
  contextPrototype.clip = function() {
    // TODO: Implement
  };

  contextPrototype.arcTo = function() {
    // TODO: Implement
  };

  contextPrototype.createPattern = function() {
    return new CanvasPattern_;
  };

  // Gradient / Pattern Stubs
  function CanvasGradient_(aType) {
    this.type_ = aType;
    this.radius1_ = 0;
    this.radius2_ = 0;
    this.colors_ = [];
    this.focus_ = {x: 0, y: 0};
  }

  CanvasGradient_.prototype.addColorStop = function(aOffset, aColor) {
    aColor = processStyle(aColor);
    this.colors_.push({offset: 1-aOffset, color: aColor});
  };

  function CanvasPattern_() {}

  // set up externs
  G_vmlCanvasManager = G_vmlCanvasManager_;
  CanvasRenderingContext2D = CanvasRenderingContext2D_;
  CanvasGradient = CanvasGradient_;
  CanvasPattern = CanvasPattern_;

})();

} // if


/**json2.js***/


/**util.js***/


function submitExample(ricepaper, uchar)
{
    json = ricepaper.character.jsonify();
    dojo.xhrGet({
        url: "/shufa/hanzi/store/example?&char=" + uchar + "&strokes=" + json,
        load: function(data)
        {
            submittedExample(data);
        }
    });   
}

function submittedExample(data)
{
    
}

function jsonMagic(data){
        csdiv = document.getElementById("charactersDiv");
        csdiv.innerHTML = "";

        eval("var chars = " + data + ";");
        //console.log("magician")
        //console.log(chars)
        for(var i =0; i < chars.length; i++)
        {
                cd = document.createElement('div');
                cd.innerHTML = chars[i].character;
                cd.setAttribute('class', 'charbox');
                csdiv.appendChild(cd);
        }
        if (chars.length <= 0 || !chars.length)
        {
                csdiv.innerHTML = "Mei you";
        }
}



function castPoint(evt)
{
    x = evt.layerX;
    y = evt.layerY;
    return {x:x,y:y};
}   

function geoToPoint(gp)
{
    return {'x':gp[0],'y':gp[1]};
}   

function jsonPoint(point)
{
    //return "{\"x\":" + point.x + ",\"y\":" + point.y + "}";
    return "[" + point.x + "," + point.y + "]";
}   

function jsonStroke(stroke)
{
    s = "["
    for(var i = 0; i < stroke.length; i++)
    {
        s += jsonPoint(stroke[i]) + ",";
    }   
    s = s.substr(0, s.length -1) + "]";
    return s;
}   

function tegakiPoint(point)
{
    p = new Point(point.x, point.y);
    return p;
}

function tegakiStroke(stroke)
{
    //writing = Writing();
    var s = new Stroke()
    for(var i = 0; i < stroke.length; i++)
    {
        s.appendPoint(tegakiPoint(stroke[i]));
    }
    return s
}

function zinniaPoint(point)
{
    return "(" + point.x + " " + point.y + ")";
}

function zinniaStroke(stroke)
{
    s = "(" 
    for(var i = 0; i < stroke.length; i++)
    {
        s += zinniaPoint(stroke[i]);
    }
    s += ")"
    return s
}

function getStyle(element,attribute,isInt){
    /*if isInt is set to 1, then return the found style as an int*/
    var a;
    //console.log(element);
    //console.log(attribute);
    //console.log(window.getComputedStyle(element,attribute));
    if(window.getComputedStyle)
    {
        a = window.getComputedStyle(element,attribute).getPropertyValue(attribute);
    }
    else
    {
        a = eval("element.currentStyle."+attribute);
    }
    return (isInt==1)?parseInt(a):a
}


/**webCanvas.js***/
/*
* Copyright (C) 2008 The Tegaki project contributors
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License along
* with this program; if not, write to the Free Software Foundation, Inc.,
* 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

/* 
* Contributors to this file:
*  - Mathieu Blondel
*  - Shawn M Moore
*/

/* Internal canvas size */
CANVAS_WIDTH = 1000;
CANVAS_HEIGHT = 1000;

WebCanvas = function(canvas) {
    this.canvas = canvas;
    this.ctx = canvas.getContext("2d");
    
    if (document.all) {
        /* For Internet Explorer */
        this.canvas.unselectable = "on";
        this.canvas.onselectstart = function() { return false };  
        this.canvas.style.cursor = "default";
    }
        
    this.internal2real_scalex = canvas.width * 1.0 / CANVAS_WIDTH;
    this.internal2real_scaley = canvas.height * 1.0 / CANVAS_HEIGHT;
    
    this.real2internal_scalex = 1.0 / this.internal2real_scalex;
    this.real2internal_scaley = 1.0 / this.internal2real_scaley;
    
    this.writing = new Writing();
    this.buttonPressed = false;
    this.first_point_time = null;
    this.locked = false;
    
    this._initListeners();
}

WebCanvas.prototype._withHandwritingLine = function() {
    this.ctx.strokeStyle = "rgb(0, 0, 0)";
    this.ctx.lineWidth = 8;
    this.ctx.lineCap = "round";
    this.ctx.lineJoin = "round";
}

WebCanvas.prototype._withStrokeLine = function() {
    this.ctx.strokeStyle = "rgba(255, 0, 0, 0.7)";
    this.ctx.lineWidth = 8;
    this.ctx.lineCap = "round";
    this.ctx.lineJoin = "round";
}

WebCanvas.prototype._withAxisLine = function() {
    this.ctx.strokeStyle = "rgba(0, 0, 0, 0.1)";
    this.ctx.lineWidth = 4;
    //this.ctx.set_dash ([8, 8], 2);
    this.ctx.lineCap = "butt";
    this.ctx.lineJoin = "round";
}

WebCanvas.prototype._drawBackground = function() {
    this.ctx.beginPath();
    this.ctx.clearRect(0, 0, CANVAS_WIDTH, CANVAS_HEIGHT);
    this.ctx.closePath();
}

WebCanvas.prototype._drawAxis = function() {
    this.ctx.save();

    this._withAxisLine();

    this.ctx.beginPath();

    this.ctx.moveTo(CANVAS_WIDTH / 2, 0);
    this.ctx.lineTo(CANVAS_WIDTH / 2, CANVAS_HEIGHT);
    this.ctx.moveTo(0, CANVAS_HEIGHT / 2);
    this.ctx.lineTo(CANVAS_WIDTH, CANVAS_HEIGHT / 2);

    this.ctx.stroke();
    this.ctx.restore();
}

WebCanvas.prototype._initListeners = function() {
    
    function callback(webcanvas, func) {
        /* Without this trick, "this" in the callback refers to the canvas HTML object.
                          With this trick, "this" refers to the WebCanvas object! */
        return function(event) {
            func.apply(webcanvas, [event]);
        }
    }
    
    if (this.canvas.attachEvent) {
        this.canvas.attachEvent("onmousemove",
                                callback(this, this._onMove));
        this.canvas.attachEvent("onmousedown",
                                callback(this, this._onButtonPressed));
        this.canvas.attachEvent("onmouseup",
                                callback(this, this._onButtonReleased));                               
        this.canvas.attachEvent("onmouseout",
                                callback(this, this._onButtonReleased));                                     
    }
    else if (this.canvas.addEventListener) {
        // Browser sniffing is evil, but I can't figure out a good way to ask in
        // advance if this browser will send touch or mouse events.
        // If we generate both touch and mouse events, the canvas gets confused
        // on iPhone/iTouch with the "revert stroke" command
        console.log(navigator.userAgent.toLowerCase());
        if (navigator.userAgent.toLowerCase().indexOf('iphone')!=-1) {
            // iPhone/iTouch events
            this.canvas.addEventListener("touchstart",
                                        callback(this, this._onButtonPressed),
                                                false);
            this.canvas.addEventListener("touchend",
                                        callback(this, this._onButtonReleased),
                                        false);
            this.canvas.addEventListener("touchcancel",
                                        callback(this, this._onButtonReleased),
                                        false);
            this.canvas.addEventListener("touchmove",
                                        callback(this, this._onMove), false);

            // Disable page scrolling via dragging inside the canvas
            this.canvas.addEventListener("touchmove", function(e){e.preventDefault();}, false);
        }else if(navigator.userAgent.toLowerCase().indexOf('android')!=-1){
            //android 4.4이상 버전에서 마우스 이벤트 웹뷰에서 인식 안되는 문제 해결
            this.canvas.addEventListener("touchstart",
                                        callback(this, this._onButtonPressed),
                                                false);
            this.canvas.addEventListener("touchend",
                                        callback(this, this._onButtonReleased),
                                        false);
            this.canvas.addEventListener("touchcancel",
                                        callback(this, this._onButtonReleased),
                                        false);
            this.canvas.addEventListener("touchmove",
                                        callback(this, this._onMove), false);
        }else{
            this.canvas.addEventListener("mousemove",
                                        callback(this, this._onMove), false);
            this.canvas.addEventListener("mousedown",
                                        callback(this, this._onButtonPressed),
                                                false);
            this.canvas.addEventListener("mouseup",
                                        callback(this, this._onButtonReleased),
                                        false);
            this.canvas.addEventListener("mouseout",
                                        callback(this, this._onButtonReleased),
                                        false);
        }
    }
    else
        alert("Your browser does not support interaction.");
}

WebCanvas.prototype._onButtonPressed = function(event) {
    
    if (this.locked) return;

    // this can occur with an iPhone/iTouch when we try to drag two fingers
    // on the canvas, causing a second smaller canvas to appear
    if (this.buttonPressed) return;

    this.buttonPressed = true;

    var position = this._getRelativePosition(event);

    var point = new Point();
    point.x = Math.round(position.x * this.real2internal_scalex);
    point.y = Math.round(position.y * this.real2internal_scaley);
    
    this.ctx.save();
    this.ctx.scale(this.internal2real_scalex, this.internal2real_scaley);    
    this._withHandwritingLine();
    this.ctx.beginPath();
    this.ctx.moveTo(point.x, point.y);
    
    var now = new Date();

    if (this.writing.getNStrokes() == 0) {
        this.first_point_time = now.getTime();
        point.timestamp = 0;
    }
    else {
        if (this.first_point_time == null) {
            /* in the case we add strokes to an imported character */
            this.first_point_time = now.getTime() -
                                    this.writing.getDuration() - 50;
        }
        
        point.timestamp = now.getTime() - this.first_point_time;
    }
    
    this.writing.moveToPoint(point);
}

WebCanvas.prototype._onButtonReleased = function(event) {
    
    if (this.locked) return;

    if (this.buttonPressed) {
        this.buttonPressed = false;
        this.ctx.restore();

        /* Added for tests only. Smoothing should be performed on a copy. */
        if (this.writing.getNStrokes() > 0){
            this.writing.getStrokes()[this.writing.getNStrokes() - 1].smooth();
            this.draw();
        }
    }
}

WebCanvas.prototype._onMove = function(event) {
    
    if (this.locked) return;

    if (this.buttonPressed) {
        var position = this._getRelativePosition(event);

        var point = new Point();
        point.x = Math.round(position.x * this.real2internal_scalex);
        point.y = Math.round(position.y * this.real2internal_scaley);        
                        
        this.ctx.lineTo(point.x, point.y);
        this.ctx.stroke();      
    
        var now = new Date();
    
        point.timestamp = now.getTime() - this.first_point_time;

        this.writing.lineToPoint(point);
    }
}

WebCanvas.prototype._getRelativePosition = function(event) {
    var t = this.canvas;

    var x, y;
    // targetTouches is iPhone/iTouch-specific; it's a list of finger drags
    if (event.targetTouches) {
       var e = event.targetTouches[0];

       x = e.pageX;
       y = e.pageY;
    }
    else {
        x = event.clientX + (window.pageXOffset || 0);
        y = event.clientY + (window.pageYOffset || 0);
    }

    do
        x -= t.offsetLeft + parseInt(t.style.borderLeftWidth || 0),
        y -= t.offsetTop + parseInt(t.style.borderTopWidth || 0);
    while (t = t.offsetParent);

    return {"x":x,"y":y};
}

WebCanvas.prototype._drawWriting = function(length) {
    var nstrokes = this.writing.getNStrokes();
    
    if (!length) length = nstrokes;

    if (nstrokes > 0) {
        var strokes = this.writing.getStrokes();

        this.ctx.save();
        
        this._withHandwritingLine();   
        
        for(var i = 0; i < length; i++) {
            var stroke = strokes[i];

            var first_point = stroke.getPoints()[0];

            this.ctx.beginPath();
            
            this.ctx.moveTo(first_point.x, first_point.y);
            
            for (var j = 1; j < stroke.getNPoints(); j++) {
                var point = stroke.getPoints()[j];
                
                this.ctx.lineTo(point.x, point.y);
            }

            this.ctx.stroke();
        }

        this.ctx.restore();
        
    }
}

WebCanvas.prototype._drawWritingAnimation = function(default_speed) {
    var nstrokes = this.writing.getNStrokes();

    if (nstrokes > 0) {
        var strokes = this.writing.getStrokes();
        
        this.ctx.save();     
        this.ctx.scale(this.internal2real_scalex, this.internal2real_scaley);   
        this._withStrokeLine();        

        var currstroke = 0;
        var currpoint = 0;
        var state = this.getLocked();
        this.setLocked(true);
        var webcanvas = this; // this inside _onAnimate doesn't refer to the web canvas
        
        _onAnimate = function() {
            
            var point = strokes[currstroke].getPoints()[currpoint];
            
            if (currpoint == 0) {                
                webcanvas.ctx.beginPath();
                webcanvas.ctx.moveTo(point.x, point.y);
            }
            else {
                webcanvas.ctx.lineTo(point.x, point.y);
                webcanvas.ctx.stroke();
            }

            if (strokes[currstroke].getNPoints() == currpoint + 1) {
                // if we reach the stroke last point
                                                                                   
                currpoint = 0;
                currstroke += 1;    

                // redraw completely the strokes we have
                webcanvas._drawBackground();
                webcanvas._drawAxis();
                webcanvas._drawWriting(currstroke); 
                
                if (strokes.length == currstroke) {
                    // if we reach the last stroke
                    webcanvas.ctx.restore();
                    webcanvas.setLocked(state);
                    return;
                }
                else {
                    // there are still strokes to go...
                }
                                   
            }
            else {
                currpoint += 1;
            }

            var delay;

            if (default_speed == null &&
                strokes[0].getPoints()[0].timestamp != null) {

                if (currpoint == 0 && currstroke == 0) {
                    // very first point
                    delay = 0;
                }
                else if (currstroke > 0 && currpoint == 0) {
                    // interstroke duration
                    var t2 = strokes[currstroke].getPoints()[0].timestamp;
                    var last_stroke = strokes[currstroke - 1].getPoints();
                    var t1 = last_stroke[last_stroke.length - 1].timestamp;
                    delay = (t2 - t1);
                }
                else {
                    var pts = strokes[currstroke].getPoints()
                    delay = pts[currpoint].timestamp -
                            pts[currpoint-1].timestamp;
                }
            }
            else
                delay = default_speed;

            setTimeout(_onAnimate, delay);
        }
        
        _onAnimate.call();        
    }
}

WebCanvas.prototype.getWriting = function() {
    return this.writing;
}

WebCanvas.prototype.setWriting = function(w) {
    if (this.locked) return;

    this.writing = w;
    this.draw();
}

WebCanvas.prototype.clear = function() {
    if (this.locked) return;

    this.setWriting(new Writing());
}

WebCanvas.prototype.draw = function() {
    this.ctx.save();

    this.ctx.scale(this.internal2real_scalex, this.internal2real_scaley);

    this._drawBackground();
    this._drawAxis();

    this._drawWriting();
    
    this.ctx.restore();
}

WebCanvas.prototype.replay = function(speed) {
    if (this.locked) return;

    this.ctx.save();

    this.ctx.scale(this.internal2real_scalex, this.internal2real_scaley);

    this._drawBackground();
    this._drawAxis();

    this.ctx.restore();
    
    this._drawWritingAnimation(speed);   
}

WebCanvas.prototype.revertStroke = function() {
    if (this.locked) return;

    if (this.writing.getNStrokes() > 0) {
        this.writing.removeLastStroke();
        this.draw();
    }
}

WebCanvas.prototype.getLocked = function() {
    return this.locked;
}

WebCanvas.prototype.setLocked = function(locked) {
    this.locked = locked;
}

WebCanvas.prototype.toDataURL = function(contentType) {
    if (this.locked) return;

    if (this.canvas.toDataURL) {       
        return this.canvas.toDataURL(contentType);
    }
    else
        return null;
}

WebCanvas.prototype.toPNG = function() {
    return this.toDataURL("image/png");
}

WebCanvas.prototype.smooth = function() {
    if (this.locked) return;

    if (this.writing.getNStrokes() > 0) {
        this.writing.smooth();
        this.draw();
    }
}

function randOrd(){return (Math.round(Math.random())-0.5); }

function ErrorCode(response){
    console.log(response);
    switch (response.status) {
      case 0    :
            Ext.Msg.alert('죄송합니다!','서버와 연결할 수 없어요.');
            break;
      case 400    :
            if(response.responseText.match("(IntegrityError)"))
                Ext.Msg.alert("확인해주세요!","이미 등록된 이메일이네요.");
            else Ext.Msg.alert("죄송합니다!",'서버 오류에요.');
            break;
      default    : Ext.Msg.alert('오류가 발생했습니다.', response.status);
                   break;
    }
}

document.addEventListener('deviceready', function () {
  document.addEventListener('backbutton', function (event) {
        Ext.Msg.confirm('',"어플리케이션을 종료하시겠습니까?", function(btn){
                  if (btn == 'yes')
                       navigator.app.exitApp();
        });
  }, false);
}, false);